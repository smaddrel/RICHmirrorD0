"""D0 Measurement for Mirrors from the lab."""

__author__ = "Sam Maddrell-Mander, Lakshan Ram Madhan Mohan"
__copyright__ = "University of Bristol, August 2018"


import cv2
import numpy as np

import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from PIL import Image

import time
import optparse
import glob
import rawpy
from pathlib import Path
import shutil
import os
import csv
import sys
import select


class ImageInfo(object):

    def __init__(self, file, ref_d0):
        """
        File name convention 'identifier'_CURRmA-'x'_DTOMm-'x'__'other_info'.'ext'

        CURR: current to laser is given in mA ; DTOM: Distance to mirrors is given in meters.
        For dark images: dark_images_'identifier'__'otherinfo'.'ext'

        Decimal places of the values are separated by ',' example: 13.0 is written as 13,0 in the convention.

        :param file: The path to a file with file name in the format of the above given convention
        """
        self._file_path = file
        self._folder_path, self._file_name = os.path.split(self._file_path)

        convention_error = ValueError('Unknown convention for file name please check documentation to use the ' +
                                      'correct convention.')

        if self._file_name.find('dark_images') is not -1:
            laser_cond = 'Dark_Image'

            tag = self._file_name.find('dark_images')+1
            d_underscore = self._file_name.find('__')

            identifier = self._file_name[tag+1:d_underscore]
            current = 0
            dist_to_mirr = 0
            if d_underscore is -1:
                print(self.get_filepath())
                raise convention_error
        else:

            try:
                f_underscore = self._file_name.find('_')

                curr_ind = self._file_name.find('CURRmA-')+7

                if self._file_name.find('DTOMm-') is not -1:
                    dtoom_ind = self._file_name.find('DTOMm-')+6
                    conversion_const = 1
                elif self._file_name.find('DTOMmm-') is not -1:
                    dtoom_ind = self._file_name.find('DTOMmm-')+7
                    conversion_const = 1000
                else:
                    raise convention_error

                s_underscore = self._file_name.find('_', curr_ind+1)
                t_underscore = self._file_name.find('_', dtoom_ind+1)
                identifier = self._file_name[0:f_underscore]
                current = float(self._file_name[curr_ind:s_underscore].replace(',', '.'))
                dist_to_mirr = float(self._file_name[dtoom_ind:t_underscore].replace(',', '.'))/conversion_const

            except ValueError:
                print(self.get_filepath())
                raise convention_error

            if f_underscore is -1 or curr_ind is -1 or dtoom_ind is -1 or s_underscore is -1 or t_underscore is -1:
                print(self.get_filepath())
                raise convention_error
            else:
                laser_cond = 'Laser_Image'

        self._laser_cond = laser_cond
        self._identifier = identifier
        self._name = identifier[:identifier.find('-')]
        self._orientation = identifier[identifier.find('-')+1:]
        self._current = current
        self._dist_to_mirr = dist_to_mirr
        self._d0 = -0.00001
        self._is_flat = False
        self._ref_d0 = ref_d0
        self._roc = 0

    def get_filepath(self):
        return self._file_path

    def get_folderpath(self):
        return self._folder_path

    def get_filename(self):
        return self._file_name

    def get_image_type(self):
        return self._laser_cond

    def get_current(self):
        return self._current

    def get_dist_to_mirr_in_m(self):
        return self._dist_to_mirr

    def change_dist_to_mirror_in_m(self, new_dist_to_mirror_in_m):

        if self._file_name.find('DTOMm-') is not -1:
            new_filepath = self._file_path.replace(str(self._dist_to_mirr).replace('.', ','),
                                                   str(new_dist_to_mirror_in_m).replace('.', ','))
            print(new_filepath)
        elif self._file_name.find('DTOMmm-') is not -1:
            new_filepath = self._file_path.replace(str(self._dist_to_mirr*1000),
                                                   str(new_dist_to_mirror_in_m*1000))

            print(new_filepath)

        print(f'Distance to mirror changed from {self._dist_to_mirr} to {new_dist_to_mirror_in_m} m')
        # self._dist_to_mirr = new_dist_to_mirror_in_m
        os.rename(self._file_path, new_filepath)
        self._file_path = new_filepath
        self._dist_to_mirr = new_dist_to_mirror_in_m

    def get_identifier(self):
        return self._identifier

    def set_d0(self, d0):
        if self._is_flat:
            self._d0 = 0.5*(d0-self._ref_d0)  # in meters
        else:
            self._d0 = d0

    def get_d0_in_m(self):
        return self._d0

    def get_d0_in_mm(self):
        return self._d0*1000

    def set_is_flat(self, is_flat):
        self._is_flat = is_flat

    def get_is_flat(self):
        return self._is_flat

    def set_roc(self, roc):
        self._roc = roc

    def get_roc(self):
        return self._roc

    def get_name(self):
        return self._name

    def get_orientation(self):
        return self._orientation


class Data(object):
    """docstring for Data."""

    def __init__(self, path, background_folder_path=None):

        self.pixel_width = 6.4e-6  # m
        self.y_pixels = 3456
        self.x_pixels = 2304

        self.ref_d0 = 0.00023  # m
        self.ref_f = 7.8/2  # m
        self.ref_dist = 2.949  # m

        self.path = path

        if background_folder_path is not None:
            self.read_background_data(background_folder_path)
        else:
            self.mean_background_grid = None
            self.std_background_grid = None

        self.dark_images = []

    def background_image_manager(self, grid):
        self.dark_images.append(grid)

    def store_background_info(self, mean_background_grid, std_background_grid):
        self.mean_background_grid = mean_background_grid
        self.std_background_grid = std_background_grid

    def write_background_data(self, folder_path):
        mean_filename = folder_path + '/Mean_Grid_data.npy'
        std_filename = folder_path + '/Std_Grid_data.npy'

        np.save(mean_filename, self.mean_background_grid)
        np.save(std_filename, self.std_background_grid)

    def read_background_data(self, folder_path):
        mean_filename = folder_path + '/Mean_Grid_data.npy'
        std_filename = folder_path + '/Std_Grid_data.npy'
        self.mean_background_grid = np.load(mean_filename)
        self.std_background_grid = np.load(std_filename)

    def background_subtraction(self, grid, sub_option='mean'):
        """Subtract the background from the image."""
        subtract_options = {'mean': 0, '1sigma': 1, '2sigma': 2, '3sigma': 3,
                            '-1sigma': -1, '-2sigma': -2, '-3sigma': -3}

        sub_grid = self.mean_background_grid + subtract_options[sub_option]*self.std_background_grid
        grid = np.subtract(grid, sub_grid)
        return grid

    def get_files(self, image_type):
        """ Reads the file names and store the info through ImageInfo class"""
        filenames = glob.glob(self.path + '/*.' + image_type, recursive=False)
        files = [ImageInfo(file=f, ref_d0=self.ref_d0) for f in filenames]
        self.n_files = len(files)
        if self.n_files is 0:
            raise FileNotFoundError('No file found in this folder. Folder maybe empty or options might not match')
        return files

    def load_data(self, image_type='RAW'):
        """Load the data files, yield each image."""

        # Using RAWPY to read CR2 images at the moment
        files = self.get_files(image_type)

        for f in files:
            name = f.get_filepath()

            if image_type == 'RAW':
                raw = rawpy.imread(f'{name}')
                grid = raw.raw_image  # with border

            elif image_type == 'CR2':
                raw = rawpy.imread(f'{name}')
                grid = raw.postprocess()
                r, g, b = cv2.split(grid)
                grid = cv2.merge([b, g, r])
                grid = cv2.cvtColor(grid, cv2.COLOR_BGR2GRAY).astype('float32')

            elif image_type == 'TIF':
                im = Image.open(name)
                grid = np.array(im)
                r, g, b = cv2.split(grid)
                grid = cv2.merge([b, g, r])
                grid = cv2.cvtColor(grid, cv2.COLOR_BGR2GRAY).astype('float32')

            elif image_type == 'JPG':
                im = Image.open(name)
                grid = np.array(im)
                r, g, b = cv2.split(grid)
                grid = cv2.merge([b, g, r])
                grid = cv2.cvtColor(grid, cv2.COLOR_BGR2GRAY).astype('float32')

            else:
                raise TypeError('Input image of unknown type')

            yield grid, f

        print('\nAll Done!')

    def plot_calculation_circle(self, grid, im_info, x0, y0, radius_in_pixels_list, results_dir=None):
        if results_dir is None:
            my_path = self.path
        else:
            my_path = results_dir

        out_filepath = my_path + '/' + im_info.get_filename()[:-4] + '_D0Circles.pdf'
        fig1 = plt.figure()

        plt.imshow(grid, cmap='hot', vmin=0)
        ax = fig1.gca()
        d0 = im_info.get_d0_in_mm()
        ax.set_title(f'{im_info.get_identifier()} \n' +
                     f'Distance to mirror: {im_info.get_dist_to_mirr_in_m():.3f} m; D0: {d0:.2f} mm')

        x = np.arange(0, (grid.shape[0])*self.pixel_width*1000, self.pixel_width*1000)
        nx = x.shape[0]
        no_labels_x = 7  # how many labels to see on axis x
        step_x = int(nx / (no_labels_x - 1))  # step between consecutive labels
        x_positions = np.arange(0, nx, step_x)  # pixel count at label position
        x_labels = x[::step_x]  # labels you want to see
        x_labels = [f'{lx:.0f}'for lx in x_labels]

        y = np.arange(0, (grid.shape[1]) * self.pixel_width * 1000, self.pixel_width * 1000)
        ny = y.shape[0]
        no_labels_y = 9  # how many labels to see on axis y
        step_y = int(ny / (no_labels_y - 1))  # step between consecutive labels
        y_positions = np.arange(0, ny, step_y)  # pixel count at label position
        y_labels = y[::step_y]  # labels you want to see
        y_labels = [f'{ly:.0f}' for ly in y_labels]

        plt.yticks(x_positions, x_labels)  # The inversion of x/y is a plotting feature
        plt.xticks(y_positions, y_labels)  # The inversion of x/y is a plotting feature
        plt.xlabel('Width (mm)')
        plt.ylabel('Height (mm)')
        for r in radius_in_pixels_list:
            colour = 'y' if r is radius_in_pixels_list[-1] and d0 is not -0.01 else 'w'
            circle = plt.Circle((y0, x0), r, ec=colour, fill=False)  # The inversion of x/y is a plotting feature
            ax.add_artist(circle)

        plt.savefig(out_filepath)
        plt.close()

    def rad_of_curv(self, dist_to_mirror, is_flat):
        if not is_flat:
            roc = dist_to_mirror
        else:
            R_ref = 2*self.ref_f
            a = self.ref_dist
            b = dist_to_mirror
            print('The RoC is approximate.')
            roc = 2/((1/b)-(1/(R_ref-a)))
            # calculation_term = m-f2*(m+dist_to_mirror)/(m+dist_to_mirror-f2) # old method
            # numerator = dist_to_mirror*calculation_term
            # denominator = (dist_to_mirror + calculation_term)
            # roc = 2*(numerator/denominator)
        return roc

    def plot_d0_variation(self, im_info_list, current_study=False, results_dir=None, combined_result_filepath=None):
        if results_dir is None:
            my_path = self.path
        else:
            my_path = results_dir

        out_filepath = my_path + f'/{im_info_list[0].get_identifier()}_D0_variation.pdf'

        d0_list = np.array([i.get_d0_in_mm() for i in im_info_list]) # in mm

        fig = plt.figure()
        ax = fig.gca()

        if current_study:
            current_list = np.array([i.get_current() for i in im_info_list])
            ax.scatter(current_list, d0_list, s=5)
            d0_minarg = int(np.argmin(d0_list))
            d0_min = d0_list[d0_minarg]
            current_min = current_list[d0_minarg]

            ax.set_title(f'{im_info_list[0].get_identifier()} \nD0 = {d0_min:.3f} mm for '+
                         f'current: {current_min:.3f} mA ')
            ax.set_xlabel('Current (mA)')
            ax.set_ylabel('D0 (mm)')

        else:
            dtom_list = np.array([i.get_dist_to_mirr_in_m() for i in im_info_list])
            # ax.set_ylim(bottom=0, top=0.004)
            ax.scatter(dtom_list, d0_list, s=5)
            false_d0_index = np.argwhere(d0_list < 0)
            d0_list = np.delete(d0_list, false_d0_index)
            im_info_list = np.delete(im_info_list, false_d0_index)
            d0_minarg = int(np.argmin(d0_list))

            d0_min = d0_list[d0_minarg]
            dtom_min = dtom_list[d0_minarg]
            roc = im_info_list[d0_minarg].get_roc()
            write_result_to_file(combined_result_filepath, im_info_list[d0_minarg])

            ax.set_title(f'{im_info_list[0].get_identifier()} \nMinimum D0 = {d0_min:.2f} mm for RoC: {roc:.3f} m')
            ax.set_xlabel('Distance to Mirror (m)')
            ax.set_ylabel('D0 (mm)')

        plt.savefig(out_filepath)
        plt.close()

    def plot_bg_variation(self, filename_suffix='', results_dir=None):
        """Quantify the background variation."""  # TODO make plot prettier
        if results_dir is None:
            my_path = self.path
        else:
            my_path = results_dir
        out_filepath = my_path + '/Background_profile_' + filename_suffix + '.pdf'
        f, (ax1, ax2) = plt.subplots(1, 2, sharey=False, figsize=(20, 15))

        f.suptitle(f'mean: {np.mean(self.mean_background_grid):.3f} '
                   '+/-' + f'{np.std(self.mean_background_grid):.3f}; '
                   f'std_dev: {np.mean(self.std_background_grid):.3f} '
                   '+/-' + f'{np.std(self.std_background_grid):.3f}')

        ax1.set_title('Mean background image')
        mean = ax1.imshow(self.mean_background_grid, norm=LogNorm(vmin=np.min(self.mean_background_grid),
                                                                  vmax=np.max(self.mean_background_grid)))
        f.colorbar(mean, ax=ax1)

        ax2.set_title(r'1 $\sigma$ background image')
        std = ax2.imshow(self.std_background_grid, norm=LogNorm(vmin=np.min(self.std_background_grid),
                                                                vmax=np.max(self.std_background_grid)))
        f.colorbar(std, ax=ax2)

        plt.savefig(out_filepath)
        plt.close()
        print(f'mean: {np.mean(self.mean_background_grid):.3f} '
              '+/-' + f'{np.std(self.mean_background_grid):.3f}; '
              f'std_dev: {np.mean(self.std_background_grid):.3f} '
              '+/-' + f'{np.std(self.std_background_grid):.3f}')


class Methods(object):
    """docstring for Methods."""

    def __init__(self):
        """Init for Methods, set up dimensions and constants."""

        self.pixel_width = 6.4e-6
        self.y_pixels = 3456
        self.x_pixels = 2304

    @staticmethod
    def background_analysis(dark_images):
        return np.mean(dark_images, axis=0), np.std(dark_images, axis=0)

    @staticmethod
    def find_com(grid):
        """Find the COM of the image to integrate shells around."""

        total_sum = np.sum(grid)
        x_axis_sumr = np.sum(np.sum(grid, axis=1)*np.arange(grid.shape[0]))
        y_axis_sumr = np.sum(np.sum(grid, axis=0)*np.arange(grid.shape[1]))
        com_x = int(round(x_axis_sumr/total_sum))
        com_y = int(round(y_axis_sumr/total_sum))
        coordinates = (com_x, com_y)

        return coordinates

    def mid_point_circle_algorithm(self, grid, data, img_info, results_dir):
        """Mid Point Circle algorithm."""
        # This is a much faster implementation of the integral

        def update_grid(update_x, update_y):
            """Cache to catch for segment overlap."""
            self.integral += self.copy_grid[update_x-1, update_y-1]
            self.copy_grid[update_x-1, update_y-1] = 0

        def check_bounds(my_radius):
            if (my_radius + self.x0) > grid.shape[0]:
                return False
            elif (my_radius - self.x0) > grid.shape[0]:
                return False
            elif (my_radius + self.y0) > grid.shape[1]:
                return False
            elif (my_radius - self.y0) > grid.shape[1]:
                return False
            else:
                return True

        def update_all(update_x, update_y):
            update_grid(self.x0 + update_x, self.y0 + update_y)
            update_grid(self.x0 + update_y, self.y0 + update_x)
            update_grid(self.x0 - update_y, self.y0 + update_x)
            update_grid(self.x0 - update_x, self.y0 + update_y)
            update_grid(self.x0 - update_x, self.y0 - update_y)
            update_grid(self.x0 - update_y, self.y0 - update_x)
            update_grid(self.x0 + update_y, self.y0 - update_x)
            update_grid(self.x0 + update_x, self.y0 - update_y)

        r = min([grid.shape[0], grid.shape[1]])

        start = time.time()
        coordinates = self.find_com(grid)
        self.x0 = coordinates[0]
        self.y0 = coordinates[1]

        first = True
        self.copy_grid = np.copy(grid)
        while True:

            circles = []
            ratios = []
            background_verification = False
            d0_pixel_radius = None

            self.integral = 0
            full_integral = np.sum(self.copy_grid)
            print('', end='')
            for radius in range(r+1):
                if check_bounds(radius):

                    x = radius - 1
                    y = 0
                    dx = 1
                    dy = 1
                    err = dx - (radius << 1)
                    while x >= y:
                        update_all(x, y)
                        if err <= 0:
                            y += 1
                            err += dy
                            dy += 2
                        if err > 0:
                            update_all(x-1, y-1)
                            x -= 1
                            dx += 2
                            err += dx - (radius << 1)

                    ratio = self.integral/full_integral

                    if (radius % 100 == 0) and (radius > 0):
                        print('\r', end='')
                        print(f'Scanned to radius: {radius}, total of { ratio*100:.2f}%', end='')
                        circles.append(radius)
                        ratios.append(ratio*100)

                    if ratio > 0.95:
                        if not background_verification:
                            print('')
                            print(f'Found {ratio*100:.2f}%  percent of the integral ' +
                                  f'measured D0 = {data.pixel_width*radius*2:.4f} m')
                            img_info.set_d0(2*radius*data.pixel_width)
                            circles.append(radius)
                            ratios.append(ratio * 100)
                            d0_pixel_radius = radius
                            background_verification = True

                        elif radius > d0_pixel_radius+10:
                            print('')
                            background_sum = full_integral - self.integral
                            background_pixels = round(grid.shape[0]*grid.shape[1]-np.pi*d0_pixel_radius**2)
                            background_average = background_sum/background_pixels
                            print(f'Average background of image: {background_average}')
                            break

                else:
                    print('')
                    print('Out of bounds. Radius larger that the image')
                    background_average = 0.7
                    break

            if first:
                print('')
                print('Secondary Background subtraction')
                self.copy_grid = np.subtract(grid, background_average)  # avg
                coordinates = self.find_com(self.copy_grid)
                # self.x0 = coordinates[0]
                # self.y0 = coordinates[1]
                first = False
            else:
                break

        data.plot_calculation_circle(grid, img_info, self.x0, self.y0, circles, results_dir)
        end = time.time()
        t1 = end - start
        print(f'Time Taken: {t1:.3f} s')


def write_result_to_file(filepath, img_info):

    assert isinstance(img_info, ImageInfo)
    file_exists = os.path.isfile(filepath)
    with open(filepath, 'a', newline='') as file:
        writer = csv.writer(file)
        if not file_exists:
            writer.writerow(["Name", "Orientation", "is_flat", "D0", "RoC", "DTOM", "Current"])
            writer.writerow(["", "", "", "(mm)", "(m)", "(m)", "(mA)"])
        writer.writerow([f'{img_info.get_name()}', f'{img_info.get_orientation()}', f'{img_info.get_is_flat()}',
                         f'{img_info.get_d0_in_mm()}',
                         f'{img_info.get_roc()}', f'{img_info.get_dist_to_mirr_in_m()}', f'{img_info.get_current()}'])


def main(path_to_data, image_type, flat_mirror_flag, background_data_flag, vary_current, background_folder=None,
         results_dir=None, combined_result_filepath='./results.csv'):
    """Parse the options and process the images."""

    data = Data(path=path_to_data, background_folder_path=background_folder)
    methods = Methods()
    generator = data.load_data(image_type=image_type)
    img_info_list = []
    if flat_mirror_flag:
        print('Using flat mirror settings. ')
    if background_data_flag:
        print('Dark Images')
    else:
        print('Laser Images')
    for i, (img, img_info) in enumerate(generator):
        img_info.set_is_flat(is_flat=flat_mirror_flag)
        print('\r', end='')
        print(f'Processed: {i}/{data.n_files} images', end='')

        # print(f'Processing image: {img_info.get_filename()} \n')
        if background_data_flag:
            data.background_image_manager(img)
        else:
            print('\n'+'-'*75)
            print(f'Processing file: {img_info.get_filename()}')
            img = data.background_subtraction(img, sub_option='mean')
            methods.mid_point_circle_algorithm(img, data, img_info, results_dir=results_dir)
            img_info.set_roc(data.rad_of_curv(img_info.get_dist_to_mirr_in_m(), img_info.get_is_flat()))
            print('-'*75)
        img_info_list.append(img_info)

    if background_data_flag:
        mean_background_grid, std_background_grid = methods.background_analysis(np.array(data.dark_images))
        data.store_background_info(mean_background_grid, std_background_grid)

        # folder_path = img_info_list[0].get_folderpath()
        data.write_background_data(results_dir)
        # data.plot_bg_variation()
    else:
        data.plot_d0_variation(img_info_list, current_study=vary_current, results_dir=results_dir,
                               combined_result_filepath=combined_result_filepath)


if __name__ == '__main__':
    parser = optparse.OptionParser()
    parser.add_option('--image_type', '-i',
                      type='choice',
                      action='store',
                      dest='image_type',
                      choices=['RAW', 'CR2', 'TIF', 'JPG'],
                      default='JPG',
                      help='What type of image file are the inputs in?')
    
    parser.add_option('--background', '-b',
                      action='store_true',
                      default=False,
                      help='profile the background or scan the data?')
    
    parser.add_option('--path', '-p',
                      action='store',
                      default='dark_images',
                      help='Path to search for the images.')

    parser.add_option('--subtract_folder', '-s',
                      action='store',
                      type=str,
                      default=None,
                      help='The folder containing the mean_background_grid and std_background_grid of background data.')

    parser.add_option('--vary_current', '-c',
                      action='store_true',
                      default=False,
                      help='If given will plot D0 variation against current. ' +
                           'The user needs to make sure the given folder contains images at different currents.')

    parser.add_option('--flat', '-f',
                      action='store_true',
                      default=False,
                      help='Use this flag if measuring flat mirrors. ' +
                           'The result will take into account the double reflection using known specs of ref mirror.')

    parser.add_option('--combined_result', '-r',
                      action='store',
                      default='./results.csv',
                      help='File to save the combined results of the study.')

    (opts, args) = parser.parse_args()
    if opts.background is False and opts.subtract_folder is None:
        dark_folders = [f[0] for f in os.walk(opts.path) if 'dark' in f[0].lower()]
        laser_folders = [f[0] for f in os.walk(opts.path) if 'laser' in f[0].lower()]

        for d, l in zip(dark_folders, laser_folders):
            assert Path(d).parent == Path(l).parent
            results_path = str(Path(d).parent) + '/Results/'
            try:
                os.mkdir(results_path)
            except FileExistsError as e:
                print(e)
                print('Would you like to overwrite the file (10 second timeout)? [y]/n: ')
                inp, o, e = select.select([sys.stdin], [], [], 10)
                if inp:
                    prompt = sys.stdin.readline().strip()
                    if prompt.lower() == 'n':
                        print(f'Ignoring current data folder-{Path(d).parent}')
                        continue
                    else:
                        print('Overwriting file')
                        shutil.rmtree(results_path)
                        os.mkdir(results_path)
            try:
                main(d, opts.image_type, opts.flat, background_data_flag=True, vary_current=opts.vary_current,
                     background_folder=None, results_dir=results_path)
                main(l, opts.image_type, opts.flat, background_data_flag=False, vary_current=opts.vary_current,
                     background_folder=results_path, results_dir=results_path,
                     combined_result_filepath=opts.combined_result)

            except FileNotFoundError as e:
                print(e)
                # os.rmdir(results)
                print(f'Ignoring current data folder-{Path(d).parent}')
                continue

    else:
        main(opts.path, opts.image_type, opts.flat, opts.background, opts.vary_current, opts.subtract_folder)
