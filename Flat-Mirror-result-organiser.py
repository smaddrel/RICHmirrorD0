from matplotlib import pyplot as plt
from matplotlib.lines import Line2D
from PyPDF4 import PdfFileReader, PdfFileWriter
from PyPDF4.pdf import PageObject
import pandas as pd
import numpy as np
import os
import optparse


def plot_results(resultscsv, out_filepath=None):

    results_df = pd.read_csv(resultscsv, delimiter=',')[1:]
    color_dict = {'center': 'tab:blue', 'invcenter': 'tab:orange',
                  'left': 'tab:green', 'right': 'tab:red'}

    results_df['Color'] = results_df['Orientation'].map(color_dict)
    custom_legend = [Line2D([0], [0], marker='o', color='w', label='Center',
                            markerfacecolor=color_dict['center'], markersize=10),
                     Line2D([0], [0], marker='o', color='w', label='Inverted Center',
                            markerfacecolor=color_dict['invcenter'], markersize=10),
                     Line2D([0], [0], marker='o', color='w', label='Left',
                            markerfacecolor=color_dict['left'], markersize=10),
                     Line2D([0], [0], marker='o', color='w', label='Right',
                            markerfacecolor=color_dict['right'], markersize=10)]

    X = results_df.Name.values
    Y = pd.to_numeric(results_df['D0'])

    solid_line = np.ones(len(X))

    fig, ax = plt.subplots(2, 1, figsize=(18, 9))
    ax[0].scatter(X, Y, c=results_df['Color'].to_numpy())
    ax[0].plot(X, solid_line*2, c='k')
    ax[0].set_ylabel('D0 (mm)')
    ax[1].scatter(X, pd.to_numeric(results_df['RoC']), c=results_df['Color'].to_numpy())
    ax[1].plot(X, solid_line*90, c='k')
    ax[1].set_ylabel('RoC (m)')
    ax[1].set_xlabel('Mirror ID')
    plt.legend(handles=custom_legend)
    if out_filepath is not None:
        plt.savefig(out_filepath)


def plot_results_breakdown(results_csv, infolder, outfile):

    results_folder = [f[0] for f in os.walk(infolder) if 'Results' in f[0]]
    writer = PdfFileWriter()
    input_page = PdfFileReader(outfile).getPage(0)
    writer.addPage(input_page)
    for r in results_folder:
        filenames = [f[2] for f in os.walk(r)][0]
        try:
            d0_variation = [f for f in filenames if '_D0_variation.pdf' in f][0]
        except IndexError:
            raise print('There is no file containing a _D0_Variation.pdf.')

        while True:
            try:
                dtom = input(f'DtoM of smallest D0 for {d0_variation}: ')
                d0_spot = [f for f in filenames if dtom in f][0]
                break
            except IndexError:
                print(f'There is no file at the specified DtoM of {dtom}. Please try again.')

        file1 = PdfFileReader(f"{r}/{d0_variation}")
        file2 = PdfFileReader(f"{r}/{d0_spot}")

        page1 = file1.getPage(0)
        page2 = file2.getPage(0)

        translated_page = PageObject.createBlankPage(None, input_page.mediaBox.getWidth(),
                                                     page1.mediaBox.getHeight())
        translated_page.mergeScaledTranslatedPage(page1, 1, 100, 0)
        translated_page.mergeScaledTranslatedPage(page2, 1, 100+input_page.mediaBox.getWidth()/2, 0)

        writer.addPage(translated_page)

    with open(outfile, 'wb') as outputStream:
        writer.write(outputStream)


def result_compiler_for_latex(results_csv, outfile):
    results_df = pd.read_csv(results_csv, delimiter=',')[1:]
    reduced_df = results_df.query("Orientation == 'center' ").copy()
    reduced_df.drop(['Orientation', 'is_flat'], axis=1, inplace=True)

    def reduce_precision(df, precision, col):
        df[col] = pd.DataFrame({col: pd.to_numeric(df[col])}).round(decimals=precision)
        return df
    reduced_df = reduce_precision(reduced_df, 1, 'RoC')
    reduced_df = reduce_precision(reduced_df, 3, 'DTOM')
    reduced_df = reduce_precision(reduced_df, 3, 'D0')
    reduced_df = reduce_precision(reduced_df, 2, 'Thickness')
    reduced_df = reduced_df.replace(np.nan, '', regex=True)
    reduced_df.to_latex(outfile.replace('.tex', '-D0.tex'),
                        columns=['Name', 'D0', 'RoC', 'DTOM', 'Current'],
                        index=False, header=['Mirror Id', 'D0 (mm)', 'RoC (m)',
                                             'DtoM (m)', 'Laser Current (mA)'],
                        caption='The ')

    reduced_df.to_latex(outfile.replace('.tex', '-dimension.tex'),
                        columns=['Name','Length min','Length Max', 'Width Min', 'Width Max', 'Thickness'],
                        index=False, header=['Mirror Id',
                                             'Length Min (mm)', 'Length Max (mm)',
                                             'Width Min (mm)', 'Width Max (mm)', 'Thickness (mm)'],
                        caption='The ')


parser = optparse.OptionParser()
parser.add_option('--resultscsv', '-r',
                  action='store',
                  dest='resultscsv',
                  help='The path to the input file')

parser.add_option('--outfile', '-o',
                  action='store',
                  dest='outfile',
                  default=None,
                  help='The path to the output file')

parser.add_option('--infolder', '-i',
                  action='store',
                  dest='infolder',
                  default=None,
                  help='The path to the input file')

(opts, args) = parser.parse_args()

plot_results(opts.resultscsv, opts.outfile)
if opts.infolder is not None:
    plot_results_breakdown(opts.resultscsv, opts.infolder, opts.outfile)

# result_compiler_for_latex(opts.resultscsv, opts.outfile)