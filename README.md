# RICHmirrorD0
Measures the D0 of input images and the background variation of input images. The smallest D0 measured over various 
distances from the mirror will give us information on the radius of curvature of the mirror. 

The D0 is defined as the diameter of the circle which contains 95% of the light with its center located at the center of 
gravity of the image.

# EOS Data store location
The data and results are to be stored at /eos/project/l/lhcbwebsites/www/lhcb-rich/ , readable by everyone at LHCb. 
If you require write access to this, ask Lakshan or Marco. 
You will then be able to write to the store and you will also be able to access it through cernbox under Share with me -> lhcb-rich.

## Dependencies

This project is built on 

- Python 3.7
- numpy 1.16.3
- matplotlib 3.1.0
- scikit-image 0.15.0
- scipy 1.3.0
- rawpy 0.13.1

Revert to these versions only if latest versions do not work.

## Usage
### Background subtraction
To subtract the background from the laser images the program uses a two step method.
- The first step uses multiple dark images and finds the average value for each pixel and subtracts this grid from the 
image.To create the grid, 
    - Store all the dark images in a separate folder as in Sample/Dark\ Images/
    - From the folder RICHmirrorD0 run the command, 
    ```bash
    python3 RICHmirrorD0.py --path Sample/Dark\ Images/ --image_type JPG --background 
    ```
    - This will create binary files within the same folder with a grid containing the mean and sigma value in each pixel.
    - Just pass the path to this folder (the binary files will automatically read) when you want to subtract this grid from the laser images.
- The second step happen internally. 
    - The D0 is calculated and all points outside the D0 circle is 
    designated as background.
    - The mean value of this background is calculated and subtracted before redoing the D0 calculation. 
This two step method accounts for any variations there may be overall and in the specific image. 

### D0 measurement. 
- To calculate the D0, the program uses the midpoint circle algorithm around the center of mass of the image. 
- The circle radius is increased until the integral is 95% of the total sum of the image.
    - Store all the laser images in a folder as in Sample/Laser\ Images/
    - To subtract the global background, pass the folder containing the binary files as --subtract_folder.
    ```bash
    python3 RICHmirrorD0.py --path Sample/Laser\ Images/ --image_type JPG --subtract_folder Sample/Dark\ Images/
    ```
    - Output graphs with the image and the D0 circle will be saved in the same folder as the laser images. 
    - The variation with the distance to the mirror and D0 will also be saved there.
    - In case the mirror is a flat mirror include the flag -f to give account for the setup. 
    (see Flat Mirrors below for more info)

### Saturation Test
- Place the camera/lens system at the point of maximum intensity of the laser and take 
images while changing the current. 
- Similar to D0 measurement run with the additional flag -c so that the code plots the D0 variation with 
current and not distance. 

**Caution:**
It is the users responsibility to check the given folder contains images with varying current only and not
 distance to mirrors.
 
The resulting graphs are stored with the laser images as before.

## Data Collection Procedure

### Spherical Mirrors
![Spherical Set up](Figures/Spherical_setup.png)
- Move the jig to be approximately at the radius of curvature of the mirror from the camera/laser system.   
- Switch on the cooling and then the laser. Change the current so that the reflected light is just about visible.  
- Secure the mirror on the jig and align it such that the reflected point hits the center of the camera. 
    - Fell free to increase the current during this step but remember to keep the camera lid on to 
    avoid accidental damage to the CCD.
- Run a saturation test by changing the current at the point of maximum intensity. 
- Once the optimal laser current is chosen, take sequential images at 1 mm differences at +/- 20 mm around 
the point of maximum intensity.
- Take a dark run right after at the same conditions and analyse them using the RICHmirrorD0.py to get the D0 and the 
RoC of the mirror.

### Flat Mirrors
- For flat or large RoC mirrors, use the reference mirror in the set up to help focus the light. 
- In this case the jig is to be placed at the 4.151 m mark such that the mirror is approximately 4.3 m from the 0 m 
mark on the set up. 
    - This will mean that the camera/laser system will approximately sit at the RoC of the reference mirror. 
- Reference mirror properties have already been coded into the program.
    - RoC: 7.8 m
    - D0: 0.23 mm 

- The path of the light is as shown below.
![Flat Set up](Figures/Flat_setup.png)
- The equations below give the RoC and D0 of the flat mirror. 
```math
RoC = \frac{2}{\frac{1}{b}-\frac{1}{R_ref-a}}
```

```math
D0 = 0.5(D0_{mes} - D0_{ref})
```

* a &rarr; Distance between the reference mirror and flat mirror.
* b &rarr; Distance to Mirror from camera.
* R<sub>ref &rarr; Radius of Curvature of the reference mirror. 
* D0<sub>ref</sub> &rarr; D0 of reference mirror.
* D0<sub>mes</sub> &rarr; Measured D0.


- Given the Distance to Mirror in the file name and the --flat flag while executing, the above properties and 
equations are use to output the D0 and RoC of the mirror. 
- Switch on the cooling and then the laser. Change the current so that the reflected light is just about visible.  
- Secure the mirror on the jig and align it such that the reflected point hits the center of the camera. 
    - Fell free to increase the current during this step but remember to keep the camera lid on to 
    avoid accidental damage to the CCD.
- Run a saturation test by changing the current at the point of maximum intensity. 
- Once the optimal laser current is chosen, take sequential images at 1 mm differences at +/- 20 mm around 
the point of maximum intensity.
- After taking your data, take a dark run immediately after. 

### Automated Analysis
- It is possible to automate the entire analysis procedure. Make sure you have the dark images stored in directory with 
'dark' (case-insensitive) in it's name. And similarly 'laser' for the directory with the laser images. 

- You can now pass the parent directory using the command
  ```bash
  python3 RICHmirrorD0.py --path Sample/ --image_type JPG
  ``` 
- All the results are stored in folder called Results within Sample\ 
- Additional flags can be used for flat mirrors and saturation test runs. 

It is also possible to pass the parent of Sample/ which may contain multiple folders with data from different mirrors.
 The program will evaluate them independently.

### Tips for taking images
- Take JPG images as the code has been tested to work well with it and the file sizes are manageable.
- The software EOSutility has the option the sequentially update the file name while taking images. 
    - Check under Preferences->filename. 
    -  This option can be used in combination with text to make it follow the naming convention. 
- Tools->Timer lets you take multiple images automatically. 

 
### File naming convention
- No Spaces to be used in the file name. 
- Dark Images and Laser Images are to be kept in separate folders and processing them should be done individually.
- The parts in __bold__ are required as standard.
- The parts in _italics_ are your identifiers and variables.
  - Never use "_" (underscore) in your identifiers or for anything as they are used to separate different parts. 
  - Adhere to the units specified and use "," instead of "." to not interfere with file extensions.

#### Dark Images 
__dark_images__\__identifier__\__serialnumber_.ext

example: dark_images_R1S4-cen__0056.JPG
#### Laser Images
_identifier_\_ __CURRmA-__ _valueinmA_\_ __DTOMm-__ _valueinm_\_\__anything-else_.ext

example: OP1-cen-flat_CURRmA-30,4_DTOMm-4,568__blah.JPG

## References
[Martin Laub Thesis](http://cdsweb.cern.ch/record/966009)